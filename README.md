Monopoly
========

My Monopoly-version in Java!

This is my first take on making something in JAVA, and will probably reflect my learning in JAVA.

Everything in this is mine, and thou shalt not steal nor copy anything.
If you do, at least be so kind as to provide the right credit.

For now, I'm trying to get it to a working v1.0. Later I want to add a colorfull UI, and the option to add different
settings like Space (where the currency is liters fuel, and the citys are planets); and 'classes' of players where e.g.
 the Sprinter can move 3 extra squares, but will stop on a cornersquare (it runs into the wall).

Bye!
