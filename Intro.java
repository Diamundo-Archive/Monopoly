package monopoly;

class Intro {
    
    public static void printIntro() {
        String intro;
        
        intro = "Hello! \n" +
                "\nThis game has been built by Kevin Nauta (c) 2014." +
                "\nCredits go to Laura van de Braak, Jelmer van der Linde, Rene Brals, and others." +
                "\nAny questions? Mail to ict.devrieseschipper@hotmail.com " +
                "\nwith 'Monopoly in Java' as the subject. ";
        
                
        
        Systeem.ClearScreen();
        System.out.println(intro + "\n");
    }
}