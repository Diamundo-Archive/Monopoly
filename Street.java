package monopoly;

public class Street {
    private final String name, description;
    private final int city, houseCost;
    private int cost, numHouses, active, owner;
    private final int rent[]; //huur onbep + huizen + hotel
    
    public Street(String name, String description, int city, 
                    int cost, int houseCost, int[] rent){
        this.name = name;
        this.description = description;
        this.city = city;
        this.cost = cost;
        this.houseCost = houseCost;
        this.numHouses = 0;
        this.active = 0;
        this.owner = -1;
        this.rent = rent;
    }
    
    public String getName() {
        return name;
    }
    
    public String getDescription() {
        return description;
    }
    
    public int getCity() {
        return city;
    }
    
    public int getCost() {
        return cost;
    }
    
    public int getHouseCost() {
        return houseCost;
    }
    
    public int getNumHouses() {
        return numHouses;
    }
    
    public int getBankHuur() {
        return cost/2;
    }
    
    public int getRent() {
        return rent[this.numHouses];
    }
    
    public int getRent(int n) {
        return rent[n];
    }
    
    public int getActive() {
        return active;
    }
    
    public int getOwner() {
        return owner;
    }
    
    public void setNumHouses(int numHouses){
        this.numHouses = numHouses;
    }
    
    public void setActive(int active) {
        this.active = active;
    }
    
    public void setRent(int[] rent) {
        int j;
        for(j=0; j<6; j++){
            this.rent[j] = rent[j];
        }
    }
    
    public void setOwner(int owner) {
        this.owner = owner;
    }
    
    public void setCost(int cost) {
        this.cost = cost;
    }

    /**
     * Admin function to enter the standard board.
     * @return Street[] streets
     */
    public static Street[] fillStreets() {
        
        Street[] streets = new Street[40];
        
        streets[0] = new Street("Start", "Start vak", 0, 0, 0, new int[]{0,0,0,0,0,0} );
        streets[1] = new Street("Derpsstraat", "Ons dorp", 3, 6000, 5000, new int[]{200,1000,3000,9000,16000,25000} );
        streets[2] = new Street("Algemeen Fonds", "Pak een kaart", 0, 0, 0, new int[]{0,0,0,0,0,0} );
        streets[3] = new Street("Brink", "Ons dorp", 3, 6000, 5000, new int[]{400,2000,6000,18000,32000,45000});
        streets[4] = new Street("Belasting betalen", "Betaal $ 20.000", 0, 20000, 0, new int[]{0,0,0,0,0,0} );
        streets[5] = new Street("Station Zuid", "Stations", 1, 20000, 0, new int[]{0,0,0,0,0,0});
        streets[6] = new Street("Steenstraat", "Arnhem", 4, 10000, 5000, new int[]{600,3000,9000,27000,40000,55000});
        streets[7] = new Street("Kans", "Pak een kaart", 0, 0, 0, new int[]{0,0,0,0,0,0});
        streets[8] = new Street("Ketelstraat", "Arnhem", 4, 10000, 5000, new int[]{600,3000,9000,27000,40000,55000});
        streets[9] = new Street("Velperplein", "Arnhem", 4, 12000, 5000, new int[]{800,4000,10000,30000,45000,60000});
        
        streets[10] = new Street("Gevangenis", "De Nor", 0, 0, 0, new int[]{0,0,0,0,0,0} );
        streets[11] = new Street("Barteljorisstraat", "Haarlem", 5, 14000, 10000, new int[]{1000,5000,15000,45000,62500,75000});
        streets[12] = new Street("Electriciteitsbedrijf", "Bzz Bzz", 2, 15000, 0, new int[]{0,0,0,0,0,0});
        streets[13] = new Street("Zijlweg", "Haarlem", 5, 14000, 10000, new int[]{1000,5000,15000,45000,62500,75000});
        streets[14] = new Street("Houtstraat", "Haarlem", 5, 16000, 10000, new int[]{1200,6000,18000,50000,70000,90000});
        streets[15] = new Street("Station West", "Stations", 1, 20000, 0, new int[]{0,0,0,0,0,0});
        streets[16] = new Street("Neude", "Utrecht", 6, 18000, 10000, new int[]{1400,7000,20000,55000,75000,95000});
        streets[17] = new Street("Algemeen Fonds", "Pak een kaart", 0, 0, 0, new int[]{0,0,0,0,0,0});
        streets[18] = new Street("Biltstraat", "Utrecht", 6, 18000, 10000, new int[]{1400,7000,20000,55000,75000,95000});
        streets[19] = new Street("Vreeburg", "Utrecht", 6, 20000, 10000, new int[]{1600,8000,22000,60000,80000,100000});
        
        streets[20] = new Street("Vrij Parkeren", "Kaching!", 0, 500, 0, new int[]{0,0,0,0,0,0});
        streets[21] = new Street("A-kerkhof", "Groningen", 7, 22000, 15000, new int[]{1800,9000,25000,70000,87500,105000});
        streets[22] = new Street("Kans", "Pak een kaart", 0, 0, 0, new int[]{0,0,0,0,0,0});
        streets[23] = new Street("Groote markt", "Groningen", 7, 22000, 15000, new int[]{1800,9000,25000,70000,87500,105000});
        streets[24] = new Street("Heerestraat", "Groningen", 7, 24000, 15000, new int[]{2000,10000,30000,75000,92500,110000});
        streets[25] = new Street("Station Noord", "Stations", 1, 20000, 0, new int[]{0,0,0,0,0,0});
        streets[26] = new Street("Spui", "Den Haag", 8, 26000, 15000, new int[]{2200,11000,33000,80000,97500,115000});
        streets[27] = new Street("Plein", "Den Haag", 8, 26000, 15000, new int[]{2200,11000,33000,80000,97500,115000});
        streets[28] = new Street("Waterleiding", "Pssssh", 2, 15000, 0, new int[]{0,0,0,0,0,0});
        streets[29] = new Street("Lange poten", "Den Haag", 8, 28000, 15000, new int[]{2400,12000,36000,85000,102500,120000});
        
        streets[30] = new Street("De Rechtbank", "U bent schuldig! *BAM!*", 0, 0, 0, new int[]{0,0,0,0,0,0});
        streets[31] = new Street("Hofplein", "Rotterdam", 9, 30000, 20000, new int[]{2600,13000,39000,90000,110000,127500});
        streets[32] = new Street("Blaak", "Rotterdam", 9, 30000, 20000, new int[]{2600,13000,39000,90000,110000,127500});
        streets[33] = new Street("Algemeen Fonds", "Pak een kaart", 0, 0, 0, new int[]{0,0,0,0,0,0});
        streets[34] = new Street("Coolsingel", "Rotterdam", 9, 32000, 20000, new int[]{2800,15000,45000,100000,120000,140000});
        streets[35] = new Street("Station Oost", "Stations", 1, 20000, 0, new int[]{0,0,0,0,0,0});
        streets[36] = new Street("Kans", "Pak een kaart", 0, 0, 0, new int[]{0,0,0,0,0,0});
        streets[37] = new Street("Leidschestraat", "Amsterdam", 10, 35000, 20000, new int[]{3500,17500,50000,110000,130000,150000});
        streets[38] = new Street("Belasting betalen", "Betaal $ 10.000", 0, 10000, 0, new int[]{0,0,0,0,0,0});
        streets[39] = new Street("Kalverstraat", "Amsterdam", 10, 40000, 20000, new int[]{5000,20000,60000,140000,170000,200000});
    
        return streets;
    }

}