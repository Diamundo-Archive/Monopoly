package monopoly;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SaveGame {
    
    public static boolean WriteToFile(String location, Player[] players, Street[] streets,
            int numPlayers, int numTurns, int activePlayer, int counter) {
        
        String fileName;
        File file;
        boolean correct = false;
        int i, j;
        
        try {
            
            do {
                fileName = askNameFile(correct);
                file = new File(location+fileName+".txt");
                correct = true;
            } while( file.exists() );
            file.createNewFile();

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            try ( BufferedWriter bw = new BufferedWriter(fw)) {
                bw.write(numPlayers + " " + numTurns + " " + activePlayer + " " + (counter<0 ? -1*counter : counter) + "\n");
                
                for(i=0; i<numPlayers; i++){
                    bw.write(players[i].getName() + "\n" + players[i].getColor() + "\n");
                    bw.write(players[i].getMoney() + " " + players[i].getOutOfJailCards() + " ");
                    bw.write(players[i].getType() + " " + players[i].getInitiative() + " " );
                    bw.write(players[i].getState() + "\n");
                }
                
                for(i=0; i<40; i++){
                    bw.write(streets[i].getName() + "\n" + streets[i].getDescription() + "\n");
                    bw.write(streets[i].getCity() + " " + streets[i].getCost() + " ");
                    bw.write(streets[i].getHouseCost() + "\n");
                    for(j=0; j<6; j++){
                        bw.write( streets[i].getRent(j) + " ");
                    } bw.write("\n");
                    bw.write(streets[i].getNumHouses() + " " + streets[i].getActive() + " ");
                    bw.write(streets[i].getOwner() + "\n");
                }
            }
            
            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public static boolean checkDir(String path) {
        File dir = new File(path);
        boolean result;

        // if the directory does not exist, create it
        if ( !dir.exists() ) {
            System.out.print("Creating directory ' " + dir.getName() + " ' = ");
            result = false;
            
            try {
                System.out.println( dir.mkdirs() );
                result = true;
            } catch(SecurityException e){
                e.printStackTrace();
            }
        } else {
            result = true;
        }
        return result;
    }

    public static String askNameFile(boolean correct) {
        if( correct ) { System.out.println("ERROR! Name incompatible! "); }
        System.out.println("How do you wish to name the file? ");
        
        return Systeem.readString();
    }
    
    public static void saveGame(Player[] players, Street[] streets, int numPlayers, int numTurns, int activePlayer, int counter) {
        String path = "C:/Users/Gebruiker/Documents/NetBeansProjects/Monopoly/saves/";
        
        if( !checkDir(path) ) {
            System.out.println("Folder NOT available! ");
        } else {
            
            if( WriteToFile(path, players, streets, numPlayers, numTurns, activePlayer, counter) ) {
                System.out.println("Saving went OK!");
            } else {
                System.out.println("Saving failed... Try again!");
            }
        
        }
    }
}
