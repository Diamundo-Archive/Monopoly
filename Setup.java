package monopoly;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Setup {
    Player[] players;
    
    public static String printColors(){
        return "[ Red, Green, Blue, Yellow, Purple, White ]";
        // should take out colors that have been taken, later on.
    }
    
    public static int askPlayers() {
        int numPlayers = -1;
        Scanner intscan = new Scanner(System.in);
        
        Systeem.ClearScreen();
        while( (numPlayers<2) || (numPlayers>6) ) {
            System.out.print("How much players want to compete? ");
            try { 
                numPlayers = intscan.nextInt();
            } catch(InputMismatchException e) {
                System.out.println("   Dude! Leer typen en voer een getal in! ");
                intscan.nextLine();
            } 
            if( (numPlayers<2) || (numPlayers>6) ){
                System.out.println("   ERROR! 2 <= numPlayers <= 6 !");
            }
        }
                
        Systeem.ClearScreen();
        return numPlayers;
    }
    
    public static Player[] fillPlayers(int numPlayers) {
        int i;
        String tmpName, tmpColor;
        Player[] players = new Player[numPlayers];
        Scanner textscan = new Scanner(System.in);
        
        for( i=0; i<numPlayers; i++ ) {
            System.out.println("Player " + (i+1) + " :");
            System.out.print("Name: ");
            tmpName = textscan.nextLine();
            System.out.print("Color " + printColors() + " : ");
            tmpColor = textscan.nextLine();
            players[i] = new Player(tmpName, tmpColor);
            
        } 
        
        return players;
    }
        
}