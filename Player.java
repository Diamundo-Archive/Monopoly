package monopoly;

public class Player {
    private String name, color;
    private int money, outOfJailCards, type, initiative, state;
    
    public Player(String name, String color){
        this.name = name;
        this.color = color; // later misschien icoontje als UI erbij komt
        this.money = 20000; // $ 20.000
        this.outOfJailCards = 0; // 0 cards
        this.type = 0; //Engineer, Thief, etc
        this.initiative = 0; // orde van dobbelen : array wordt later gesorteerd. not sure if nodig of apart arraytje ff maken.
        this.state = 0; // Start
    }
    
    public String getName() {
        return name;
    }
    
    public String getColor() {
        return color;
    }
   
    public int getOutOfJailCards() {
        return outOfJailCards;
    }

    public int getMoney() {
        return money;
    }

    public int getType() {
        return type;
    }
    
    public int getInitiative() {
        return initiative;
    }
    
    public int getState() {
        return state;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setColor(String color){
        this.color = color;
    }
    
    public void setOutOfJailCards(int numCards) {
        this.outOfJailCards = numCards;
    }

    public void setMoney(int money) {
        this.money = money;
    }
    
    public void setType(int type) {
        this.type = type;
    }
    
    public void setInitiative (int init) {
        this.initiative = init;
    }
    
    public void setState(int state) {
        if(state >= 40) { //passing start
            state %= 40;
            this.setMoney( this.getMoney() + 20000);
        } else if (state<0) { //moving back past start + fine
            state += 40;
            this.setMoney( this.getMoney() - 20000);
        }
        this.state = state;
    }
    
    public static int payHouses(Street[] streets, int player, int payHouse, int payHotel) {
        int pay = 0;
        
        for(int i=0; i<40; i++){
            if( streets[i].getOwner() == player) {
                if(streets[i].getNumHouses() == 5) {
                    pay += payHotel;
                } else {
                    pay += (payHouse * streets[i].getNumHouses() );
                }
            }
        }
        return pay;
    
    }
    
}