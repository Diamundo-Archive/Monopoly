package monopoly;
import java.util.Scanner;

public class Systeem {
    
    /**
     * dirty hack: prints 35 newlines.
     */
    public static void ClearScreen() {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }
    
    /**
     * read a string as input
     * @return 
     */
    public static String readString() {
        Scanner x = new Scanner(System.in);
        return x.nextLine();
    }
    
    public static int readInt() {
        Scanner x = new Scanner(System.in);
        return x.nextInt();
    }
}
