package monopoly;

/**
 *      Idee: een 1D array gebruiken, en daar de nummers 0 t/m 18 ingooien.
 *      Schudden: de array door elkaar gooien. Dan elke keer als een kaart
 *      gepakt wordt, de waarde van Kans[teller] in de switch gooien,
 *      en dan teller = (teller + 1) % 19;
 * 
 *      WIP: Teksten zijn nog niet klaar.
 * 
 */

public class Kans {
    
    private int[] kansArr = new int[19];
    private int kaart = 0;
    
    public Kans() {
        
        for(int i=0; i<19; i++){
            this.kansArr[i] = i;
        } 
        this.kaart = 0;
//        kansArr = shuffleArr(kansArr, 19);
        
    }
    
    public String getKans() {
        
        
        return "a";
    }
    
    public void update() {
        this.kaart = (this.kaart + 1) % 19;
    }
    
    public String kansKaart(Player[] players, Street[] streets, int player, int numPlayers) {
        int pay;
        
        switch( this.kaart ){
            case 0: 
                players[player].setMoney( players[player].getMoney() - 1500);
                streets[20].setCost( streets[20].getCost() + 1500);
                return "Door rood gefietst! Foei! Paid $ 1.500"; 
            case 1: 
                players[player].setMoney( players[player].getMoney() - 19000);
                streets[20].setCost( streets[20].getCost() + 19000);
                return "Collegegeld betalen... Paid $ 19.000"; 
            case 2: 
                players[player].setMoney( players[player].getMoney() - 8000);
                streets[20].setCost( streets[20].getCost() + 8000);
                return "Boeken kopen... Paid $ 8.000"; 
            case 3: 
                players[player].setMoney( players[player].getMoney() - 2100);
                streets[20].setCost( streets[20].getCost() + 2100);
                return "Barrekening betalen... Paid $ 2.100"; 
            case 4: 
                players[player].setState( 39 );
                return "Ga verder naar " + streets[ players[player].getState() ].getName() + ".";
            case 5: 
                if( players[player].getState() > 24) { 
                    players[player].setMoney( players[player].getMoney() + 20000);
                }
                players[player].setState(24);
                return "Reis naar " + streets[24].getName() + ". Als je via Start gaat, krijg je $ 20.000"; 
            case 6: 
                players[player].setState(0);
                players[player].setMoney( players[player].getMoney() + 20000 );
                return "Hoorcollege is afgelopen! Naar de Coverkamer!"; 
            case 7: 
                players[player].setState( players[player].getState() - 3);
                return "Niet in de gang voetballen! Terug naar de deur."; 
            case 8: 
                players[player].setState(10);
                return "You downloaded a car? To prison with you!"; 
            case 9: 
                if( players[player].getState() > 15 ){
                    players[player].setMoney( players[player].getMoney() + 20000);
                }
                players[player].setState(15);
                return "Ga verder naar " + streets[ players[player].getState() ].getName(); 
            case 10: 
                players[player].setMoney( players[player].getMoney() + 5000);
                return "Gratis geld van de bank! Gained $ 5.000"; 
            case 11: 
                players[player].setOutOfJailCards( players[player].getOutOfJailCards() + 1);
                return "There's a hole behind the poster :O "; 
            case 12: 
                players[player].setOutOfJailCards( players[player].getOutOfJailCards() + 1);
                return "Get out of jail card."; 
            case 13: 
                pay = Player.payHouses(streets, player, 4000, 11500);
                players[player].setMoney( players[player].getMoney() - pay );
                streets[20].setCost( streets[20].getCost() + pay ); 
                return "Yo Mamma came home. Also, an earthquake hit the street."; 
            case 14: 
                pay = Player.payHouses(streets, player, 2500, 10000);
                players[player].setMoney( players[player].getMoney() - pay );
                streets[20].setCost( streets[20].getCost() + pay);
                return "Yo Mamma came home. Also, a thunderstorm hit the street."; 
            case 15: 
                players[player].setMoney( players[player].getMoney() + 6000);
                return "Boeken weer verkocht. Gained $ 6.000"; 
            case 16: 
                players[player].setMoney( players[player].getMoney() - 1500);
                streets[20].setCost( streets[20].getCost() + 1500);
                return "Gefietst zonder licht... Boete jongetje! Paid $ 1.500"; 
            case 17: 
                if( players[player].getState() > 11 ){
                    players[player].setMoney( players[player].getMoney() + 20000);
                }
                players[player].setState(11);
                return "Ga verder naar " + streets[ players[player].getState() ].getName(); 
            case 18: // troll kaart
                return "Je hebt het algoritme af! Goed voor jou!";
            default: 
                return "Everyone gets 5000 because the Programmer fucked up!";
        } // end of switch
    }
}
