package monopoly;
import java.util.Scanner;

public class Menu {
    
    /**
     * prints the appropriate amount of spaces to align the players' statuses.
     * @param n the amount of spaces to be printed
     * @return String space, the amount of spaces to be printed.
     */
    public static String printSpaces(int n){
        String space = "";
       
        while(n>0) {
            space += " ";
            n--;
        }
        
        return space;
    }
    
    /**
     * makes the int n readable by putting '.' every three digits from the right.
     * $ 20000 will become $ 20.000
     * @param n money to be fixed
     * @return String x: fixed money
     */
    public static String readMoney(int n) {
        String x = "";
        
        while(n > 1000) {

            if(n%1000 == 0) {
                x = "000" + x;
            } else if (n%1000 < 10) {
                x = "00" + n%1000 + x;
            } else if (n%1000 < 100) {
                x = "0" + n%1000 + x;
            } else if (n%1000 < 1000) {
                x = "" + n%1000 + x;
            }
        
            n/=1000;
            x = "." + x;
        }
        
        x = n + "" + x;
        
        return x;
    }
    
    /**
     * Prints the midgame statuses of all players next to eachother.
     * @param numPlayers number of Players
     * @param players array of players
     * @param turns number of the current round.
     * @param activePlayer number of the current player
     * @param move : output for rolling dice
     */
    public static void printStatus(int numPlayers, Player[] players, int turns, int activePlayer, String move) {
        // prints all statuses of the players with lines above and below them as a box.
        int i, k;
        String lines="", x, y;
        
        for(i=0; i< (20*numPlayers)-5; i++){
                lines += "-";
        }
        System.out.println(printSpaces( numPlayers*10 - 6 ) + "Turn " + turns + "\n" + lines);
        
        for(i=0; i<numPlayers; i++) { //Name (color)
            x = players[i].getName();
            y = players[i].getColor();
            System.out.print(x + " (" + y + ")" + printSpaces(17 - x.length() - y.length() ) );
        } System.out.println("");
        
        for(i=0; i<numPlayers; i++) { //$ money: 20.000 instead of 20000
            x = readMoney( players[i].getMoney() );
            System.out.print("$ " + x + printSpaces(18 - x.length() ) );
        } System.out.println("");
        
        for(i=0; i<numPlayers; i++) {
            k = players[i].getOutOfJailCards();
            System.out.print("Jail Cards: " + k + printSpaces( (k>9 ? 6 : 7) ) );
        } System.out.println("");
        
        for(i=0; i<numPlayers; i++) { //State
            k = players[i].getState();
            System.out.print("State: " + k + printSpaces( (k>9 ? 11 : 12) ) );
        } 
        
        System.out.println("\n" + lines);
        System.out.println( printSpaces(numPlayers*10 - 6) + players[activePlayer].getName() + "'s turn: \n");
        
        System.out.println(move);
    }
    
    /**
     * Midgame menu that asks if player wants to:
     *  - buy a street
     *  - buy houses (if all property has been sold)
     *  - end his turn
     *  - save the current game
     *  - exit w/o saving
     *  - or something else still undefined.
     * @param streets : the array of streets
     * @param players : the array of players
     * @param player : the n'th player in the array whose turn it is.
     * @param state : the n'th state in the array where we are
     * @param numPlayers : number of players
     * @param kans : n'th card of 'Kans' deck
     * @param fonds  : n'th card of 'Algemeen Fonds' deck
     * @return the chosen action.
     */
    
    public static String actionState(Street[] streets, Player[] players, int player, int state, int numPlayers, Fonds fonds, Kans kans) {
        String street = "Currently on " + streets[state].getName() + " (" + streets[state].getDescription() + "), owned by ";    
        
        if(streets[state].getCity() == 0) {
            switch( streets[state].getName() ) {
                case "Start" : 
                    street = "U untvangt $20.000!";
                    players[player].setMoney( players[player].getMoney() + 20000 );
                    break;
                case "Belasting betalen" :
                    street = "Belasting betalen! Betaal $ " + readMoney( streets[state].getCost() );
                    players[player].setMoney( players[player].getMoney() - streets[state].getCost() );
                    streets[20].setCost( streets[20].getCost() + streets[state].getCost() );
                    // sets the value of cost on VrijParkeren to itself + the value of tax to be paid
                    break;
                case "Algemeen Fonds" :
                    street = "Algemeen Fonds: " + fonds.fondsKaart(players, streets, player, numPlayers);
                    fonds.update();
                    break;
                case "Kans" :
                    street = "Kans: " + kans.kansKaart(players, streets, player, numPlayers);
                    kans.update();
                    break;
                case "De Rechtbank" : 
                    street = "U bent schuldig! Naar de gevangenis!";
                    players[player].setState(10);
                    break;
                case "Gevangenis" :
                    street = "U zit in de gevangenis...";
                    break;
                case "Vrij Parkeren" : 
                    street = "U wint de pot! U krijgt $ " + readMoney( streets[20].getCost() );
                    players[player].setMoney( players[player].getMoney() + streets[20].getCost() );
                    streets[20].setCost(0);
                    break;
                default:
                    break;
            }
        } else if(streets[state].getOwner() != -1 ) {
            if(streets[state].getOwner() != player) {
                street += players[ streets[state].getOwner()].getName() + ".";                
            } else {
                street += "you.";
            }
        } else {
            street += "no-one.";
        }
        
        return street;
    }
    
    public static String buyStreet(Street[] streets, Player[] players, int player, int state, int numPlayers, Fonds fonds, Kans kans) {
        String buyStreet = "";
        int rent;
        
        if ( streets[state].getOwner() == player ) {
            buyStreet = "Already bought by yourself.";
        } else if ( streets[state].getOwner() == -1) {
            
            if(streets[state].getCity() == 0) { // system property
                buyStreet = "Cannot be bought: System property.";
            } else {
                if( players[player].getMoney() >= streets[state].getCost() ) {
                    buyStreet = "Buy it? Costs $ " + readMoney( streets[state].getCost() ) + ".";
                } else {
                    buyStreet = "Costs $ " + readMoney( streets[state].getCost() ) + ", but not enough funds...";
                } 
            }
            
        } else if ( (streets[state].getOwner() != player) && (streets[state].getOwner() >= 0) ) {
            buyStreet = "Already bought by " + players[ streets[state].getOwner() ].getName() + ". Pay him $ " + readMoney( streets[state].getRent() );
        }
        
        return buyStreet;
    }
    
    public static void printAction(int counter, String street, String buyStreet) {
        if( counter < 0) { counter *= -1; } // check for negative counter
        if( counter > 4 ) { counter %= 5; } // check for counter OOB
        
        System.out.println( (counter==0? " > " : "   ") + street );
        System.out.println( (counter==1? " > " : "   ") + buyStreet );
        System.out.println( (counter==2? " > " : "   ") + "End your turn ");
        System.out.println( (counter==3? " > " : "   ") + "Save Game : WIP (N/A) ");
        System.out.println( (counter==4? " > " : "   ") + "End the Game without saving ");
        
        System.out.print("\nPress W/S to move cursor, or type 'enter'/'e' to select. ");
    }
    
    public static int askAction(int counter) {    
        // want to use keyPressed() but is a lot of work, this is, for now, easier to use.
        String keyboard = Systeem.readString();
        System.out.print("");

        if(counter < 0) { counter *= -1; } // case of negative counter 
        if(counter > 4) { counter %= 5; }  // case of counter OOB
        
        switch(keyboard) {  
            case "W" :
            case "w" :
                counter = (counter+4) % 5; 
                break;
            case "S" :
            case "s" :
                counter = (counter + 1) % 5;
                break;
            case "kill" :
                System.exit(0);
                break;
            case "" : // enter
            case "enter" :
            case "e" :
                return ( -1 * counter);
            default: break;
        }
    
        return counter;
    }
    
    public static void payRivalPlayer(Street[] streets, Player[] players, int player, int state){
        
        if( ( streets[state].getOwner() != player) && ( streets[state].getOwner() >= 0) ) {
            if( streets[state].getCity() != 2) {
                players[player].setMoney( players[player].getMoney() - streets[state].getRent() );
                players[ streets[ players[player].getState() ].getOwner() ].setMoney( players[ streets[ players[player].getState() ].getOwner() ].getMoney() + streets[state].getRent() );
            } else {
                // do nothing, numMovedStreets needed for electricity Company / water company
            }
        }
        
    }
}