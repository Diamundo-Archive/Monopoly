package monopoly;

/**
 *      Idee: een 1D array gebruiken, en daar de nummers 0 t/m 18 ingooien.
 *      Schudden: de array door elkaar gooien. Dan elke keer als een kaart
 *      gepakt wordt, de waarde van AlgemeenFonds[teller] in de switch gooien,
 *      en dan teller = (teller + 1) % 19;
 * 
 *      WIP: Teksten zijn nog niet klaar.
 * 
 */

/**
 * Fonds: set of cards
 * @author Kevin
 */
public class Fonds {
    
    private int[] fondsArr = new int[19];
    private int kaart=0;
    
    public Fonds() {
        
        for(int i=0; i<19; i++){
            this.fondsArr[i] = i;
        } 
        this.kaart = 0;
//        fondsArr = shuffleArr(kansArr, 19);
        
    }
    
    public void update() {
        this.kaart = (this.kaart + 1) % 19;
    }
    
    
    public String fondsKaart(Player[] players, Street[] streets, int player, int numPlayers) {
        switch( this.kaart ){
            case 0: 
                players[player].setMoney( players[player].getMoney() + 10000);
                return "Someone died, and left you $ 10.000;"; 
            case 1: 
                players[player].setMoney( players[player].getMoney() + 5000);
                return "Sell! Sell! SELL! Gained $ 5.000"; 
            case 2: 
                players[player].setMoney( players[player].getMoney() + 20000);
                return "The bank's calculator segfaulted. Gained $ 20.000"; 
            case 3: 
                players[player].setState(1);
                return "Maak een u-bocht naar Ons Dorp"; 
            case 4: 
                players[player].setState(10);
                return "You downloaded a car! To prison with you!";
            case 5:
                return "It's your birthday... Except everyone forgot. So. Yeah."; 
            case 6: 
                for(int i=0; i<numPlayers; i++) {
                    if(i != numPlayers) {
                        players[i].setMoney( players[i].getMoney() - 1000);
                    } else {
                        players[i].setMoney( players[i].getMoney() + ((numPlayers-1) * 1000) );
                    }
                }
                return "This time they remembered your birthday! Hurray! Everyone gave $ 1.000 to you!"; 
            case 7: 
                players[player].setMoney( players[player].getMoney() + 1000);
                return "You won the beautycontest. this.confidence(up); "; 
            case 8: 
                players[player].setMoney( players[player].getMoney() - 5000);
                streets[20].setCost( streets[20].getCost() + 5000);
                return "The docter wants to get paid. Paid $ 5.000"; 
            case 9: 
                players[player].setMoney( players[player].getMoney() - 5000);
                streets[20].setCost( streets[20].getCost() + 5000);
                return "The insurance company wants to get paid. Paid $ 5.000"; 
            case 10: 
                players[player].setMoney( players[player].getMoney() + 5000);
                return "A function returned money! Gained $ 5.000"; 
            case 11: 
                players[player].setOutOfJailCards( players[player].getOutOfJailCards() + 1);
                return "Cake with a shovel. Yup."; 
            case 12: 
                players[player].setOutOfJailCards( players[player].getOutOfJailCards() + 1);                
                return "Cake with TNT. KABOOM!"; 
            case 13: 
                players[player].setMoney( players[player].getMoney() + 3000);
                return "Stufi is gestort! + $ 3.000"; 
            case 14: 
                players[player].setMoney ( players[player].getMoney() - 10000);
                streets[20].setCost( streets[20].getCost() + 10000);
                return "Je bent zo dronken, dat het ziekenhuis zelfs geld wil. Waarom weet je niet meer. Paid $ 10.000"; 
            case 15: 
                players[player].setMoney( players[player].getMoney() + 10000);
                return "Lijfrente vervalt. Geen flauw idee waarom, maar geld. Gained $ 10.000"; 
            case 16: 
                players[player].setState(0);
                return "Hoorcollege is klaar! To de coverkamer!"; 
            case 17: 
                players[player].setMoney( players[player].getMoney() - 1500);
                streets[20].setCost( streets[20].getCost() + 1500);
                return "Truth or Dare! (boete of kanskaart, maar kanskaart kan niet... Dus $ 1.500 boete)"; 
            case 18: // deze doet helemaal niks
                return "Nothing to see here. Move along, move along."; 
            default: 
                for(int i=0; i<numPlayers; i++){
                    players[i].setMoney( players[i].getMoney() + 5000);
                }
                return "Everyone gets $ 5.000 because the Programmer fucked up!";
        } // end of switch
    }    
}
